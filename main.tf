resource "google_storage_bucket" "buckets" {
    count = 3
    name = "${var.name}-${(count.index)+1}"
    storage_class = var.storage_class
    location = var.region
}